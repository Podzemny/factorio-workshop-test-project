context("Stone production", () => {
  beforeEach(() => {
    cy.resetServer();
    cy.startGame("Nairobi");
  });

  it("Mining by hand", function() {
    for (let i = 0; i < 3; i++) {
      cy.findFirstStone().click();
      cy.mineResource();
    }
    cy.findFirstStone().click();
    cy.buildMine();
    cy.findSecondStone().click();
    cy.mineResource();
    cy.tickGame();

    cy.stoneDeposit().should("be.equal", 4);
  });
});
