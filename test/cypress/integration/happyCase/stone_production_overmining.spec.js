context("Stone production", () => {
  beforeEach(() => {
    cy.resetServer();
    cy.startGame("Nairobi");
  });

  it("Mining by hand", function() {
    cy.findFirstStone().trigger("mouseover");
    let countStones = cy.readTooltip().text();
    cy.log(countStones);
    cy.mineResource();

    cy.findFirstStone().click();
    cy.buildMine();
    cy.findSecondStone().click();
    cy.mineResource();
    cy.tickGame();

    cy.stoneDeposit().should("be.equal", 4);
  });
});
