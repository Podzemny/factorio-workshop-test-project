// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("startGame", playerName => {
  cy.visit("/");
  cy.get("select").select(playerName);
  cy.get("button").click(); // play as default player
});

Cypress.Commands.add("tickGame", () => {
  cy.wait(1000); // game ticks like that
});

Cypress.Commands.add("stoneDeposit", () => {
  return cy.get("#bank-Stone-amount").invoke("text");
});

Cypress.Commands.add("findFirstStone", () => {
  return cy.get('td > div > img[alt="Stone Icon"]:first');
});

Cypress.Commands.add("findSecondStone", () => {
  return cy.get('td > div > img[alt="Stone Icon"]:eq(2)');
});

Cypress.Commands.add("mineResource", () => {
  cy.get("#action-MineResource").click();
});

Cypress.Commands.add("buildMine", () => {
  cy.get("#action-BuildMine").click();
});

Cypress.Commands.add("readTooltip", () => {
  return cy.get("td > div > span:first");
});

Cypress.Commands.add("resetServer", () => {
  cy.server();
  cy.request("POST", "http://localhost:8090/restart").as("restart");
  cy.get("@restart").should(response => {
    expect(response.status).to.equals(200);
  });
});
